# Sublime Jq

[jq](https://stedolan.github.io/jq/) wrapper for [Sublime Text](https://www.sublimetext.com/) 3 & 4.

It gives you jq in the best of ways:

1. Interactively construct a jq query with live update
2. Format a JSON document (pretty print or make it compact)
3. Run pre-defined jq queries on any json document with a keyboard shortcut
4. Saves your jq command in history for reuse

## Installation

### Via Package Control

Install it from [packagecontrol.io to benefit from automatic updates](https://packagecontrol.io/packages/Jq).

### Manually

Clone this repository in your Sublime Package directory (Find it in "Preferences"
and then "Browse Packages").

## Requirements

This plugin does not work on its own. It completely depends on jq.

Therefore, before using this plugin, you must have installed [jq](https://stedolan.github.io/jq/),
and you must ensure that `jq` is in your PATH.

That's it!

## How to use it

### Interactively transform JSON with a jq query

Within a given tab, start the Sublime Command Palette
with `Ctrl + Shift + p`, and search for `jq: Transform JSON`.

An input panel will be displayed at the bottom, and your JSON will be transformed
on the fly while you write your jq query.

If you aren't happy with your query, you can cancel it at anytime with `Escape`,
and the side panel with the output will be closed.

Note:
  If you finish an interactive session with `Enter`, your query will be saved in your history.
  By default, the latest query from the history will be used next time.

![](screenshots/jq_transform.gif)

### Rerun a previous query from history

Within a given tab, start the Sublime Command Palette
with `Ctrl + Shift + p`, and search for `jq: Transform JSON from history`.

Select the query you prefer, and you'll be dropped in an interactive session
with that query directly.

### Format JSON

Within a given tab, start the Sublime Command Palette
with `Ctrl + Shift + p`, and search for `jq: Format JSON`.

Your tab content will be replaced with the formatted/pretty-printed json.

![](screenshots/jq_pretty.gif)

### Format JSON: compact

Within a given tab, start the Sublime Command Palette
with `Ctrl + Shift + p`, and search for `jq: Format JSON (compact)`.

Your tab content will be replaced with the formatted json as a 1-liner.

![](screenshots/jq_compact.gif)

## Commands Documentation

### `jq_format_json`

Given a tab, pretty prints its json content. Runs `jq '.'` to do so.

Example:
```python
view.run_command("jq_format_json")
```

Available in the command palette.

### `jq_format_json_compact`

Given a tab, format its json content on 1 line. Runs `jq --compact-output '.'` to do so.

Example:
```python
view.run_command("jq_format_json_compact")
```

Available in the command palette.

### `jq_transform_json`

Given a tab, starts an interactive session to transform its json content.
Lets you run `jq 'query'` interactively.
If the `query` parameter is provided, it serves as the initial query;
otherwise, it uses the last command from the history.

Example:
```python
view.run_command("jq_transform_json")
```

You can also bind it to a keyboard shortcut if you use it often:

```python
{ "keys": ["super+j"], "command": "jq_transform_json" },
```

Available in the command palette.

### `jq_transform_json_history`

Given a tab, shows a popup with the list of previous jq queries used.
When one is selected, starts an interactive session just like `jq_transform_json`
would, but with that previous query already filled-in.

Example:
```python
view.run_command("jq_transform_json_history")
```

Available in the command palette.

### `jq_apply_query`

Given a tab, applies a pre-defined jq query to its content.
This is useful if you often process a json the same way, or if you want to set a shortcut to it.

Example (pretty print with sorted keys):
```python
view.run_command("jq_apply_query", {"query": ".", "sort_keys": True})
```

Same example, but assigns the keyboard shortcut `Super + j` to it:
```python
{ "keys": ["super+j"], "command": "jq_apply_query", "args": {"query": ".", "sort_keys": True} },
```

`jq_apply_query` accepts the following arguments:


| Argument   | Values        | Default Value | Jq correspondence   |
|------------|---------------|---------------|---------------------|
| query      | string        | Mandatory     | jq 'query'          |
| compact    | True or False | False         | jq --compact-output |
| slurp      | True or False | False         | jq --slurp          |
| sort_keys  | True or False | False         | jq --sort-keys      |
| raw_in     | True or False | False         | jq --raw-input      |
| raw_out    | True or False | False         | jq --raw-output     |
| null_input | True or False | False         | jq --null-input     |


Unavailable in the command palette, only available as a command in the console or
when assigned as a keyboard shortcut.
