from threading import Timer

import sublime
import sublime_plugin

import subprocess
import os

def run_jq(text, query, line_endings,
           compact=False,
           slurp=False,
           sort_keys=False,
           raw_in=False,
           raw_out=False,
           null_input=False):
    options = []
    if compact:
        options.append("--compact-output")
    if slurp:
        options.append("--slurp")
    if sort_keys:
        options.append("--sort-keys")
    if raw_in:
        options.append("--raw-input")
    if raw_out:
        options.append("--raw-output")
    if null_input:
        options.append("--null-input")

    command = ["jq"] + options + [query]

    startup_info = None

    if sublime.platform().lower() == "windows":
        # Avoid showing console on windows (faster, and less flashy)
        startup_info = subprocess.STARTUPINFO()
        startup_info.dwFlags |= subprocess.STARTF_USESHOWWINDOW

    try:
        proc = subprocess.Popen(command,
                          stdin=subprocess.PIPE,
                          stdout=subprocess.PIPE,
                          stderr=subprocess.PIPE,
                          startupinfo=startup_info)
    except FileNotFoundError as e:
        print(e)
        message = "SublimeJq plugin: Jq not found. Install it, and make it available in your $PATH."
        message += "\n\nJq can be found at https://stedolan.github.io/jq/"
        sublime.error_message(message)
        return text

    try:
        out, err = proc.communicate(input=bytes(text, "utf-8"), timeout=10)
    except subprocess.TimeoutExpired:
        proc.kill()
        sublime.error_message("SublimeJq plugin: jq process killed, because it took more than 10s")
        return text
    if proc.returncode > 0:
        output = err
    elif proc.returncode == 0:
        output = out
    else:
        return text

    formatted_text = output.decode("utf-8")
    if line_endings.lower() == "unix":
        formatted_text = formatted_text.replace("\r\n", "\n")
    return formatted_text


def set_syntax_to_json(view):
    sublime_version = int(sublime.version())
    if 3000 <= sublime_version < 4000:
        view.set_syntax_file("Packages/JavaScript/JSON.sublime-syntax")
    if sublime_version >= 4000:
        view.assign_syntax("scope:source.json")


def is_plugin_enabled(view):
    sublime_version = int(sublime.version())
    if 3000 <= sublime_version < 4050:
        return True
    if sublime_version >= 4050:
        if view.element() is not None:
            return False
    return True


class JqHistory:
    __instance = None

    @staticmethod
    def instance():
        """
        Keep the same instance as long as Sublime is running.
        If not, different instances will start holding different values
        between saving and showing the history.
        """
        if JqHistory.__instance is None:
            return JqHistory()
        else:
            return JqHistory.__instance

    def __init__(self):
        self._max_size = 50
        self._filename = JqHistory._full_file_path()
        self._values = self._load_from_history_file()
        JqHistory.__instance = self

    @staticmethod
    def _full_file_path():
        sublime_version = int(sublime.version())
        current_directory = os.path.dirname(os.path.realpath(__file__))
        if 3000 <= sublime_version < 4050:
            current_directory = os.path.abspath(os.path.join(current_directory, os.pardir))
            if sublime.platform().lower() == "windows":
                return current_directory + "\\history.txt"
            else:
                return current_directory + "/history.txt"
        if sublime_version >= 4050:
            from pathlib import Path
            directory = Path(current_directory)
            return str(directory.parent.joinpath("history.txt"))

    def _load_from_history_file(self):
        try:
            with open(self._filename, "r", encoding="utf-8") as fd:
                return fd.read().splitlines()
        except FileNotFoundError:
            try:
                with open(self._filename, "w", encoding="utf-8") as fd:
                    fd.writelines([])
            except Exception as e:
                print("Unable to create a history file", e)
            return []
        except Exception as e:
            print(e)
            message = "SublimeJq plugin: cannot load history file. Open a defect."
            sublime.error_message(message)
            return []

    def _save_to_history_file(self):
        try:
            with open(self._filename, "w", encoding="utf-8") as fd:
                fd.writelines("\n".join(self._values))
        except Exception as e:
            print(e)
            message = "SublimeJq plugin: cannot save history file. Open a defect."
            sublime.error_message(message)

    def save(self, query):
        if query in self._values:
            return
        self._values.append(query)
        while len(self._values) > self._max_size:
            self._values.pop(0)
        self._save_to_history_file()

    def values(self):
        """Return the historical values, ordered by descending time"""
        return self._values[::-1]


class JqApplyQueryCommand(sublime_plugin.TextCommand):
    def run(self, edit, query,
            compact=False,
            slurp=False,
            sort_keys=False,
            raw_in=False,
            raw_out=False,
            null_input=False):
        size = self.view.size()
        region = sublime.Region(0, size)

        set_syntax_to_json(self.view)

        text = self.view.substr(region)
        formatted_text = run_jq(text, query,
                                self.view.line_endings(),
                                compact=compact,
                                slurp=slurp,
                                sort_keys=sort_keys,
                                raw_in=raw_in,
                                raw_out=raw_out,
                                null_input=null_input)
        self.view.replace(edit, region, formatted_text)

    def is_enabled(self):
        return is_plugin_enabled(self.view)


class JqFormatJsonCommand(sublime_plugin.TextCommand):
    def run(self, edit):
        self.view.run_command("jq_apply_query", {"query": "."})

    def is_enabled(self):
        return is_plugin_enabled(self.view)


class JqFormatJsonCompactCommand(sublime_plugin.TextCommand):
    def run(self, edit):
        self.view.run_command("jq_apply_query", {"query": ".", "compact": True})

    def is_enabled(self):
        return is_plugin_enabled(self.view)


class JqSetTextCommand(sublime_plugin.TextCommand):
    def run(self, edit, text):
        self.view.set_read_only(False)
        size = self.view.size()
        region = sublime.Region(0, size)
        self.view.replace(edit, region, text)
        self.view.set_read_only(True)


class JqTransformJsonCommand(sublime_plugin.TextCommand):
    def is_enabled(self):
        return is_plugin_enabled(self.view)

    __output_view = None
    __panel_view_id = None

    @staticmethod
    def panel_view_id():
        return JqTransformJsonCommand.__panel_view_id

    @staticmethod
    def reset_panel_view_id():
        JqTransformJsonCommand.__panel_view_id = None

    def run(self, edit, query=None):
        self.last_timer = Timer(0.0001, lambda: 0, [])
        self.last_timer.start()
        size = self.view.size()
        region = sublime.Region(0, size)
        self.original_text = self.view.substr(region)

        set_syntax_to_json(self.view)

        # Try to use the latest command from history, if any
        if query is None:
            if len(JqHistory.instance().values()) > 0:
                query = JqHistory.instance().values()[0]
            else:
                query = "."

        two_columns = {'cells': [[0, 0, 1, 1], [1, 0, 2, 1]],
                       'cols': [0.0, 0.5, 1.0],
                       'rows': [0.0, 1.0]}
        self.view.window().set_layout(two_columns)
        self.__output_view = self.view.window().new_file()
        self.__output_view.set_scratch(True)
        self.__panel_view_id = self.view.window().show_input_panel("jq query", query, self.on_done,
                                                                   self.on_change, self.on_cancel)

    def on_done(self, query):
        # Only save the query if the user allows it (with Enter)
        # Otherwise, it's difficult to know which queries to keep
        # due to the interactive process
        JqHistory.instance().save(query)
        JqTransformJsonCommand.reset_panel_view_id()
        self.__output_view = None
        return

    def on_change(self, query: str):
        # Run jq in the background, but only when the user stops typing for 250 ms
        # Aka, cancel the previous query in all cases
        self.last_timer.cancel()
        self.last_timer = Timer(0.25, self.trigger_jq, [query])
        self.last_timer.start()

    def trigger_jq(self, query):
        self.__output_view.run_command("jq_set_text", {
            "text": run_jq(self.original_text, query, self.view.line_endings())
        })

    def on_cancel(self):
        # Reset text to the original one
        self.__output_view.close()
        one_column = {'cells': [[0, 0, 1, 1]],
                      'cols': [0.0, 1.0],
                      'rows': [0.0, 1.0]}
        self.view.window().set_layout(one_column)
        JqTransformJsonCommand.reset_panel_view_id()
        self.__output_view = None


# The history of the jq query cannot be displayed in the input panel directly
# So a new command is needed to show the list of previous commands instead
# Waiting for https://github.com/sublimehq/sublime_text/issues/4040 to be fixed
# to implement a better UX
class JqTransformJsonHistoryCommand(sublime_plugin.TextCommand):
    def run(self, edit):
        self.view.window().show_quick_panel(JqHistory.instance().values(),
                                            self.on_select,
                                            sublime.MONOSPACE_FONT)

    def on_select(self, history_index):
        if history_index == -1:
            return
        query = JqHistory.instance().values()[history_index]
        self.view.run_command("jq_transform_json", {"query": query})



class JqViewListener(sublime_plugin.ViewEventListener):
    """
    Close the jq input panel if the user clicks on another view
    """

    @classmethod
    def is_applicable(self, settings):
        # No way to determine if applicable just from settings
        return True

    def on_activated(self):
        if JqTransformJsonCommand.panel_view_id() is None:
            return
        # hide the input panel if the view that just got in focus
        # is not the view of the jq panel.
        # We cannot use on_deactivated, because it gets called
        # right away when the input panel appears
        if self.view.id() != JqTransformJsonCommand.panel_view_id():
            self.view.window().run_command("hide_panel", {"cancel": True})
            JqTransformJsonCommand.reset_panel_view_id()
            # note: show_input_panel returns a view, but it doesn't seem to be a real view,
            # and .close() doesn't do anything on it.
            # the "hide_panel" command is the one called when the user hits the escape key
            # and, so, we re-use that
